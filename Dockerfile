FROM golang:1.13.0-stretch

COPY . go/src/myapp

RUN go get github.com/revel/revel
RUN go get github.com/revel/cmd/revel

ENTRYPOINT revel run myapp dev 8080

EXPOSE 8080
