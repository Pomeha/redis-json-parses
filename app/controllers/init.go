package controllers

import (
	"os"

	"github.com/gomodule/redigo/redis"
)

var RedisPool *redis.Pool

func newPool() *redis.Pool {
	return &redis.Pool{
		// Maximum number of idle connections in the pool.
		MaxIdle: 80,
		// max number of connections
		MaxActive: 12000,
		// Dial is an application supplied function for creating and
		// configuring a connection.
		Dial: func() (redis.Conn, error) {
			c, err := redis.Dial("tcp", os.Getenv("REDIS_URL"))
			if err != nil {
				panic(err.Error())
			}
			return c, err
		},
	}
}

func init() {
	RedisPool = newPool()
}
