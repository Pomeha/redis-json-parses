package controllers

import (
	"github.com/gomodule/redigo/redis"
	"github.com/revel/revel"
)

type App struct {
	*revel.Controller
}

func (c App) Index() revel.Result {
	conn := RedisPool.Get()
	defer conn.Close()

	result, err := redis.String(conn.Do("GET", "recent_posts"))

	if err != nil {
		panic(err)
	}

	c.Response.WriteHeader(200, "application/json")

	return c.RenderText(result)
}
