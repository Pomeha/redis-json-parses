package services

import (
	"github.com/go-redis/redis"
)

func GetFromRedisByKey(key string) string {
	client := redis.NewClient(&redis.Options{
		Addr: ENV("REDIS_URL"),
	})

	data, err := client.Get(key).Result()
	if err != nil {
		panic(err)
	}

	return data
}
